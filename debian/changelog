libterm-clui-perl (1.76-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.76.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 Nov 2019 19:58:28 +0100

libterm-clui-perl (1.75-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.75.
  * Move some packages from Depends to Recommends.
    Term::ReadKey and Term::ReadLine::Gnu are optional.
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Dec 2018 20:40:06 +0100

libterm-clui-perl (1.73-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Axel Beckert ]
  * Import upstream version 1.73
  * Update debian/upstream/metadata from META.json.
  * Update lintian overrides for changed line numbers.
  * Bump debhelper compatibility level to 11.
    + Update versioned debhelper build-dependency accordingly.
  * Set "Rules-Requires-Root: no".
  * Declare compliance with Debian Policy 4.1.5. (No changes needed.)

 -- Axel Beckert <abe@debian.org>  Sat, 14 Jul 2018 18:27:37 +0200

libterm-clui-perl (1.71-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Axel Beckert ]
  * Import upstream version 1.71.
  * Declare compliance with Debian Policy 4.0.0. (No changes needed.)
  * Bump debhelper compatibility to 10.
    + Update versioned debhelper build-dependency accordingly.

 -- Axel Beckert <abe@debian.org>  Mon, 19 Jun 2017 02:24:03 +0200

libterm-clui-perl (1.70-1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Add debian/upstream/metadata
  * Import upstream version 1.70
    + Update line number in lintian override.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtestable.

 -- Axel Beckert <abe@debian.org>  Fri, 03 Jul 2015 00:38:24 +0200

libterm-clui-perl (1.69-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 12 Jul 2014 21:08:24 +0200

libterm-clui-perl (1.68-1) unstable; urgency=low

  * Team upload.
  * New upstream release

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * Add lintian override for perl-module-uses-perl4-libs-without-dep
  * Bump Standards-Version to 3.9.4 (no changes)
  * Bump debhelper compatibility to 9
    + Update versioned debhelper compatibility accordingly
    + Gets rid of the dh_pysupport deprecation warning

 -- Axel Beckert <abe@debian.org>  Sat, 28 Sep 2013 04:13:53 +0200

libterm-clui-perl (1.66-1) unstable; urgency=low

  * Initial Release. (Closes: #689504)

 -- Mathieu Malaterre <malat@debian.org>  Wed, 03 Oct 2012 13:13:52 +0200
